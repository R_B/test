package test;

import java.util.List;

import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Demo1 {

	@Test
	public void getData1() {
		RestAssured.baseURI = "https://sv443.net/jokeapi/v2";
		RequestSpecification reqs = RestAssured.given();
		reqs.expect().defaultParser(Parser.JSON);
		Response response = reqs.request("GET","/categories");	
		Jokes jok = response.getBody().as(Jokes.class);
		List<String> categ = jok.getCategories();
		System.out.println(categ.size());
		System.out.println(categ);
	}
	
	// Using ObjectMapper class
	
	@Test
	public void getData2() throws JsonMappingException, JsonProcessingException {
		RestAssured.baseURI = "https://sv443.net/jokeapi/v2";
		RequestSpecification reqs = RestAssured.given();
		Response response = reqs.request("GET","/categories");
		ObjectMapper objMapper = new ObjectMapper();
		Jokes jokes = objMapper.readValue(response.asString(), Jokes.class);
		System.out.println(jokes.getTimestamp());
	}
	
	@Test
	public void postData1() throws JsonProcessingException {
		RestAssured.baseURI = "https://sv443.net/jokeapi/v2/submit";
		RequestSpecification reqs = RestAssured.given();
		
		Flags flag = new Flags();
		flag.setNsfw(true);
		flag.setPolitical(false);
		flag.setRacist(false);
		flag.setReligious(false);
		flag.setSexist(false);
		
		SubmitJokes sj  = new SubmitJokes();
		sj.setCategory("Miscellaneous");
		sj.setFormatVersion(2);
		sj.setJoke("Hello is there any flag missing ?");
		sj.setType("single");
		sj.setFlags(flag);
		
		ObjectMapper objMapper = new ObjectMapper();
		String json = objMapper.writeValueAsString(sj);
		reqs.body(json);
		
		Response response = reqs.request("PUT");
		System.out.println(response.asString());
	}
}
